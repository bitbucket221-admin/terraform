terraform {
  backend "remote" {
    organization = "remote-hub"

    workspaces {
      name = "remote"
    }
  }
required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
}

  required_version = ">= 0.14.9"
}
provider "aws" {
    region = "${var.AWS_REGION}"
    profile = "default" #given aws credentials in default location in C: drive (no need to give path)
}

resource "aws_vpc" "tf-vpc" {
    cidr_block = "${var.AWS_VPC_CIDR}"
    enable_dns_support = "true" #gives you an internal domain name
    enable_dns_hostnames = "true" #gives you an internal host name
    enable_classiclink = "false"
    instance_tenancy = "default"

    tags  = {
        Name = "tf-vpc"
    }
}
resource "aws_subnet" "tf-subnet-public-1" {
    vpc_id = "${aws_vpc.tf-vpc.id}"
    cidr_block = "${var.PUBLIC_SUBNET_1_CIDR}"
    map_public_ip_on_launch = "true" //it makes this a public subnet
    availability_zone = "${var.PUBLIC_SUBNET_1_AZ}"
    tags  = {
        Name = "tf-subnet-public-1"
    }
}
resource "aws_subnet" "tf-subnet-public-2" {
    vpc_id = "${aws_vpc.tf-vpc.id}"
    cidr_block = "${var.PUBLIC_SUBNET_2_CIDR}"
    map_public_ip_on_launch = "true" //it makes this a public subnet
    availability_zone = "${var.PUBLIC_SUBNET_2_AZ}"
    tags  = {
        Name = "tf-subnet-public-2"
    }
}
resource "aws_subnet" "tf-subnet-public-3" {
    vpc_id = "${aws_vpc.tf-vpc.id}"
    cidr_block = "${var.PUBLIC_SUBNET_3_CIDR}"
    map_public_ip_on_launch = "true" //it makes this a public subnet
    availability_zone = "${var.PUBLIC_SUBNET_3_AZ}"
    tags  = {
        Name = "tf-subnet-public-3"
    }
}

resource "aws_subnet" "tf-subnet-private-1" {

    vpc_id = "${aws_vpc.tf-vpc.id}"
    cidr_block = "${var.PRIVATE_SUBNET_1_CIDR}"
    map_public_ip_on_launch = "false" //it makes this a public subnet
    availability_zone = "${var.PRIVATE_SUBNET_1_AZ}"
    tags  = {
        Name = "tf-subnet-private-1"
    }
}
resource "aws_subnet" "tf-subnet-private-2" {

    vpc_id = "${aws_vpc.tf-vpc.id}"
    cidr_block = "${var.PRIVATE_SUBNET_2_CIDR}"
    map_public_ip_on_launch = "false" //it makes this a public subnet
    availability_zone = "${var.PRIVATE_SUBNET_2_AZ}"
    tags  = {
        Name = "tf-subnet-private-2"
    }
}
resource "aws_subnet" "tf-subnet-private-3" {

    vpc_id = "${aws_vpc.tf-vpc.id}"
    cidr_block = "${var.PRIVATE_SUBNET_3_CIDR}"
    map_public_ip_on_launch = "false" //it makes this a public subnet
    availability_zone = "${var.PRIVATE_SUBNET_3_AZ}"
    tags  = {
        Name = "tf-subnet-private-3"
    }
}

resource "aws_internet_gateway" "tf-igw" {
    vpc_id = "${aws_vpc.tf-vpc.id}"
    tags  = {
        Name = "tf-igw"
    }
}
resource "aws_route_table" "tf-public-rt" {
    vpc_id = "${aws_vpc.tf-vpc.id}"

    route {
        //associated subnet can reach everywhere
        cidr_block = "0.0.0.0/0"
        //CRT uses this IGW to reach internet
        gateway_id = "${aws_internet_gateway.tf-igw.id}"
    }

    tags  = {
        Name = "tf-public-rt"
    }
}

resource "aws_route_table_association" "tf-rta-public-subnet-1"{
    subnet_id = "${aws_subnet.tf-subnet-public-1.id}"
    route_table_id = "${aws_route_table.tf-public-rt.id}"
}
resource "aws_route_table_association" "tf-rta-public-subnet-2"{
    subnet_id = "${aws_subnet.tf-subnet-public-2.id}"
    route_table_id = "${aws_route_table.tf-public-rt.id}"
}
resource "aws_route_table_association" "tf-rta-public-subnet-3"{
    subnet_id = "${aws_subnet.tf-subnet-public-3.id}"
    route_table_id = "${aws_route_table.tf-public-rt.id}"
}

resource "aws_nat_gateway" "tf-nat" {
  connectivity_type = "private"
  subnet_id         = aws_subnet.tf-subnet-public-1.id
  tags  = {
      Name = "tf-nat"
  }
}

resource "aws_route_table" "tf-private-rt" {
    vpc_id = "${aws_vpc.tf-vpc.id}"

    route {
        //associated subnet can reach everywhere
        cidr_block = "10.0.101.0/24"

        gateway_id = "${aws_nat_gateway.tf-nat.id}"
    }

    tags  = {
        Name = "tf-private-rt"
    }
}

resource "aws_route_table_association" "tf-rta-private-subnet-1"{
    subnet_id = "${aws_subnet.tf-subnet-private-1.id}"
    route_table_id = "${aws_route_table.tf-private-rt.id}"
}
resource "aws_route_table_association" "tf-rta-private-subnet-2"{
    subnet_id = "${aws_subnet.tf-subnet-private-2.id}"
    route_table_id = "${aws_route_table.tf-private-rt.id}"
}
resource "aws_route_table_association" "tf-rta-private-subnet-3"{
    subnet_id = "${aws_subnet.tf-subnet-private-3.id}"
    route_table_id = "${aws_route_table.tf-private-rt.id}"
}


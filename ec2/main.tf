provider "aws" {
  region = "us-east-1"
  profile = "default"  #given aws credentials in default location in C: drive (no need to give path)
}
resource "aws_instance" "node" {
#  count = 2
  ami          = "ami-0eb5f3f64b10d3e0e"
  instance_type = "t2.micro"
  tags = {
    Name = var.instance_name
  }
}
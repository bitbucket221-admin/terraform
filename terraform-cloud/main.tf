
terraform {
  backend "remote" {
    organization = "remote-hub"

    workspaces {
      name = "remote"
    }
  }
required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
}

  required_version = ">= 0.14.9"
}

provider "aws" {
  region  = "us-east-1"
  profile = "default"  #given aws credentials in default location in C: drive (no need to give path)
}

resource "aws_instance" "app_server" {
  ami           = "ami-0eb5f3f64b10d3e0e"
  instance_type = "t2.micro"
}

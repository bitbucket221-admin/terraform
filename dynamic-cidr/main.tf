provider "aws" {
  region = "us-east-1"
  profile = "default"  #given aws credentials in default location in C: drive (no need to give path)
}

resource "aws_vpc" "main" {
  cidr_block = "${var.vpc_cidr}"
  instance_tenancy = "default"
  tags = {
    Name = "main"
  }
}

resource "aws_subnet" "subnet" {
  count = 4
  vpc_id     = "${aws_vpc.main.id}"
  cidr_block = "${cidrsubnet(var.vpc_cidr,8,count.index )}"
  tags = {
    Name = "subnet"
  }
}

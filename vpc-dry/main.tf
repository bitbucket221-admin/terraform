provider "aws" {
  region = "us-east-1"
  profile = "default"  #given aws credentials in default location in C: drive (no need to give path)
}

resource "aws_vpc" "tf-vpc" {
  cidr_block = var.vpc_cidr

  tags = {
    Name        = "tf-vpc"
  }
}

resource "aws_subnet" "public" {
  for_each = var.public_subnet_numbers

  vpc_id            = aws_vpc.tf-vpc.id
  availability_zone = each.key


  cidr_block = cidrsubnet(aws_vpc.tf-vpc.cidr_block, 4, each.value)
}


resource "aws_subnet" "private" {
  for_each = var.private_subnet_numbers

  vpc_id            = aws_vpc.tf-vpc.id
  availability_zone = each.key


  cidr_block = cidrsubnet(aws_vpc.tf-vpc.cidr_block, 4, each.value)

}

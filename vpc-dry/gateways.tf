resource "aws_internet_gateway" "tf-igw" {
  vpc_id = aws_vpc.tf-vpc.id

  tags = {
    Name        = "tf-igw"
  }
}

resource "aws_eip" "tf-eip" {
  vpc = true

  lifecycle {
    # prevent_destroy = true
  }

  tags = {
    Name        = "tf-eip"

  }
}

resource "aws_nat_gateway" "tf-ngw" {
  allocation_id = aws_eip.tf-eip.id
  subnet_id = aws_subnet.public[element(keys(aws_subnet.public), 0)].id

  tags = {
    Name        = "tf-ngw"

  }
}

resource "aws_route_table" "tf-public-rt" {
  vpc_id = aws_vpc.tf-vpc.id

  tags = {
    Name        = "tf-public-rt"

  }
}

resource "aws_route_table" "tf-private-rt" {
  vpc_id = aws_vpc.tf-vpc.id

  tags = {
    Name        = "tf-private-rt"

  }
}

resource "aws_route" "tf-public-rt" {
  route_table_id         = aws_route_table.tf-public-rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.tf-igw.id
}

resource "aws_route" "tf-private-rt" {
  route_table_id         = aws_route_table.tf-private-rt.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.tf-ngw.id
}

resource "aws_route_table_association" "public" {
  for_each  = aws_subnet.public
  subnet_id = aws_subnet.public[each.key].id

  route_table_id = aws_route_table.tf-public-rt.id
}

resource "aws_route_table_association" "private" {
  for_each  = aws_subnet.private
  subnet_id = aws_subnet.private[each.key].id

  route_table_id = aws_route_table.tf-private-rt.id
}